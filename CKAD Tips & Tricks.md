# My CKAD Tips & Tricks

:information_source: These tips were written right after my examination in 2020. Things may have changed since...

## Constraints

During the exam, we have access to both the official documentation on [kubernetes.io](https://kubernetes.io/) and the [official Kubernetes repository on Github](https://github.com/kubernetes/kubernetes).

:warning: you are only allowed to open 2 tabs in your browser. One with both the questions and the embed terminal, the second one with one of the above documentation. You can navigate from one documentation to the other, but you are never allowed to open more than 2 tabs. And its tighly monitored.

:gift: My personal advice here: if you need to go through the Kubernetes code then... it is already too late :skull:. Reading code takes a lot of time. And you probably don't have that time.

## Content

There are 19 questions, which must be answered within 2 hours. This is a lot of questions. You must be fast.

The 14 or 15 first questions are all on the same context (*read Kubernetes context here, meaning that the `pods`, `deployments`, `services`, ... are all the same.*) But you may have several `namespaces`. So beware of the namespaced resources. And the `namespace` you are targetting with your queries.

The 4 to 5 next questions are on different Kubernetes contextes.

All queries must be run in the same embed terminal.

Context switching command line does not need to be known. It is provided in the header of the question. You just have to copy and paste it in the terminal.

:warning: the copy/paste command shortcuts are not ctrl-C/ctrl-V on PC (or command-C/command-V on Mac). On PC, it was Ctrl-Insert and Shift-Insert for me. Keep in mind that difference. It may save you some precious time.

## Setup

Immediately `su` (set user) to `root` in your terminal. I know. This is highly discouraged in a "normal" world. But you are not in your environment here, but in a learning sandbox. And you will need to be `root` to access some files. No benefits for you to maintain a different user and switch to `root` only when needed. You would miss your shortcuts, aliases, ...

Take a couple of seconds to setup what you will really need:

- setup your most useful terminal aliases,
- setup `kubectl` autocomplete (it's in the doc, right here: [kubectl autocomplete](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-autocomplete)). Just copy and paste,
- setup your text editor. For example, your YAML files should be indented with 2 spaces each time you inset a `<tab>`. I'm a big fan of VIM. I have that setup: [VIM convert tabs to spaces](https://linuxhandbook.com/vim-indentation-tab-spaces/),
- setup anything else that you need/use/are familiar with and would save valuable time during the exam. But remember, the clock :clock: has already started and you have 19 questions to answer...

## Questions

:information_source: obviously, it is forbidden to provide the exact questions. And would be unfair. So below, you will only find general tips regarding questions.

Not all questions are rated the same. But for every question, its percentage of the global note is provided. So take a look at that before spending too much time on a question. Might not be worth spending it.

First question is the easy one. This is the appetizer. Mine was worth 13% of all the points while just requiring to setup a single simple pod (like starting a nginx, a busybox or something like this.) No challenge here. You can see it as your "Hello world :earth_africa:" in Kubernetes. You must go very fast on that one. Less than 5 minutes.

For all other questions, their weight on the global note is very heterogeneous. Some are worth only 3% while you may need more than 10 minutes to complete. Skip them. You will always be able to go back once you have covered all the exam if you still have time for these little extra.

You do not need to always write the Kubernetes manifest. Sometimes you must. In all cases, you must have an accurate knowledge of them, how they are structured. It is really important to master them.

Also, mastering the CLI (`kubectl run`, `kubectl create`, `kubectl apply`, `kubectl expose`, ...) is absolutely mandatory. Or you will waste a lot of time.

:gift: My personal advice here: run your commands with the `--dry run` option and redirect the output to a file (eg: `kubectl run xxx --dry-run > my-pod.yaml`). Then you can edit the file and apply it (eg: `kubectl apply -f my-pod.yaml`). This will save you precious time. And prevent many typos...

You will have to edit Kubernetes objects (`pods`, `deployments`, ...). Save them before editing. (eg: `kubectl get pods edit_me -o yaml --export > backup-edit_me.yaml`). Once again, this makes reading the object definition way easier, and more important... rolling back to a stable state almost instantaneous. Otherwise, you may struggle in finding what you mistyped which broke the object.

Do not get me wrong. Do not confuse "speed" and "urgency". The timeframe is short. So you do not have time... to waste your time. So **take your time to read accurately the questions**. If you are asked to provide a *summary* or a *description*... in a JSON file, then you are asked to run `kubectl get xxx -ojson > xxx.json` **not** `kubectl describe ...`.

Remind that many objects in Kubernetes are namespaced. And consequently most commands are also namespaced.
So when you need to find which pod is failing, it might help to use the `--all-namespaces` option.

Similarly, when you need to identify a pod related to a deployment, first describe the deployment with `kubectl describe XXX` to see its labels; second show the labels of the pods to identify them accurately with `kubectl get pods --show-labels`.

I had a question on monitoring. Do not forget that `kubectl top` command is rather helpful in this case.

Kubernetes is not only `deployments`, `pods` and `services`. You may have to worry about... `CronJob`, `Ingress` or `NetworkPolicy`. They are all in the official doc, of course. And you do not need to absolutely master them. But you need to know that these are "standard" objects in Kubernetes, with `metadata`, but also `spec.podSelector.matchLabels` for example in the case of `NetworkPolicy`... and you have to demonstrate that you know where and how to find this information. And then eventually applying it to `pods` to change their behavior.

## Conclusion

I hope this helps.

:four_leaf_clover: All in all, I wish you good luck. 🤞